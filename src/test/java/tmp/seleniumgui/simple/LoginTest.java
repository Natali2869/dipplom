package tmp.seleniumgui.simple;

import io.qameta.allure.*;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import tmp.seleniumgui.pages.LandingPage;
import tmp.selenium.pojo.User;


import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Tag("smoke")
@Epic("Login Tests Epic")
@Feature("Invalid Login Features")

public class LoginTest extends BaseTest {

    @Story("User tries to login the system with empty username and invalid password.")
    @DisplayName("tmp.selenium.pojo.User successfully logged in")
    @Test
    void login() {
        new LandingPage(driver)
                .open()
                .clickLogin()
                .login(Admin.getUsername(), Admin.getPassword())
                .isLoggedIn(Admin.getUsername());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "wrong_email.csv")
    void login_wrong_email(final String incorrect_email, final String errorMessage) {
        AssertionsForClassTypes.assertThat(new LandingPage(driver)
                .open()
                .clickLogin()
                .fillEmail(incorrect_email == null ? "" : incorrect_email)
                .clickEnterPassBtn()
                .getErrorText()).isEqualTo(errorMessage);

    }

    @Tag("screenshot")
    @DisplayName("Mailbox element should be equal to save image")
    @Test
    void compareImage(){
           assertThat(new LandingPage(driver)
                   .open()
                   .checkMailBoxIsDifferByImage())
                   .as("Mailbox is not equal to etalon image")
                   .isFalse();
    }

    @Test
    void  scrollTest(){
      assertThat(new LandingPage(driver).open().scrollToAbout()).isEqualTo("О компании");
    }


        }
        return str;


