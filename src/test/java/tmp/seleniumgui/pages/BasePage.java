package tmp.seleniumgui.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
  public   WebDriver driver;
    public  BasePage(WebDriver driver){
        this.driver = driver;
    }

    public void scrollTo(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }


    }
    public void waitVisible(WebElement element, int seconds) {
        new WebDriverWait(driver, Duration.ofSeconds(seconds))
                .until(ExpectedConditions.visibilityOf(new WebDriverWait(driver, Duration.ofSeconds(7))
                        .until(ExpectedConditions.visibilityOf(element))));
    }

    public void waitClickable(WebElement element, int seconds) {
        new WebDriverWait(driver, Duration.ofSeconds(seconds))
                .until(ExpectedConditions.visibilityOf(new WebDriverWait(driver, Duration.ofSeconds(7))
                        .until(ExpectedConditions.elementToBeClickable(element))));
    }

    public void scrollToByStep(WebElement element) {
        int height = 350;
        for (int i = 0; i < 10; i++) {
            testWaitMillis(100);
            if (element.isDisplayed())
                return;
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + height + ");");
            height += 350;
        }
    }

    public void testWaitMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
