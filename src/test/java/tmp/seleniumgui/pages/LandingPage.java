package tmp.seleniumgui.pages;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import tmp.selenium.pojo.User;
import tmp.seleniumgui.popups.LoginPopUp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class LandingPage extends BasePage {
    private final String URL = "https://mail.ru";

    @FindBy(css = "[data-testid='enter-mail-primary']")
    public WebElement loginBtn;
    @FindBy(css = "[data-testid='mailbox']")
    public WebElement mailBox;

    @FindBy(xpath = "(//div[contains(@class,'footer svelte')])[2]/a[2]")
    public WebElement aboutCompany;

    public LandingPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Step("Open Login page")
    public LandingPage open() {
        driver.get(URL);
        return this;
    }

    @Step("Click login")
    public LoginPopUp clickLogin() {
        waitClickable(loginBtn, 5);
        loginBtn.click();
        return new LoginPopUp(driver);
    }

    public MailBoxPage loggedInSuccessfully(User user) {
        return loggedInSuccessfully(user.getUsername(), user.getPassword());

    }

    public MailBoxPage loggedInSuccessfully(String username, String password) {
        loginBtn.click();
        return new LoginPopUp(driver).login(username, password);

    }
    @Step("Scroll to about")
    public String scrollToAbout(){
        scrollTo(aboutCompany);
        Allure.getLifecycle().addAttachment("About company", "image/png", "png",
                aboutCompany.getScreenshotAs(OutputType.BYTES));
        return aboutCompany.getText();
    }

  /*  @SneakyThrows
    public Screenshot getMailBoxImage(){
         waitVisible(mailBox, 10);
        return new AShot()
                .takeScreenshot(driver, mailBox);
         File image =  mailBox.getScreenshotAs(OutputType.FILE);
         //Files.copy(image.toPath(), new File("mailBox.png").toPath());
         return  image;
    }*/

    @Step("Compare images")
     public boolean checkMailBoxIsDifferByImage() {
        waitVisible(mailBox, 10);
         try {
             Thread.sleep(5000);
         BufferedImage screenShot = null;

             screenShot = ImageIO.read(new ByteArrayInputStream(mailBox.getScreenshotAs(OutputType.BYTES)));

         var etalonShot = ImageIO.read(Thread.currentThread().getContextClassLoader().getResourceAsStream("mailBox.png"));
             ImageDiff diff = new ImageDiffer().makeDiff(screenShot, etalonShot);
             return diff.hasDiff();
         } catch (IOException | InterruptedException e) {
             e.printStackTrace();
         }
       return false;

    }


}
