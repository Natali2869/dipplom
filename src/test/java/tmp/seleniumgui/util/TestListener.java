package tmp.seleniumgui.util;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import tmp.seleniumgui.simple.BaseTest;


public class TestListener implements TestWatcher{

  @Override
  public void  testFailed(ExtensionContext context, Throwable cause) {
      var driver = BaseTest.driver;
      String message = "Test failed["+context.getDisplayName()+"], reason"+cause.getMessage();
      Allure.getLifecycle().addAttachment("screenshot", "image/png", "png",
              ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
      System.out.println(message);

    }


}
