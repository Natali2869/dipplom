package tmp.selenium.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Config {

    class Device{
        String width;
        String height;
    }
    ObjectMapper objectMapper = new ObjectMapper();
    private  JsonNode configNode;
    private String UserName;
    private String Password;
    private String Browser;
    private String Device;

    public String getBrowser() {
        return Browser;
    }

    public void setBrowser(String browser) {
        Browser = browser;
    }

    public String getDevice() {
        return Device;
    }

    public void setDevice(String device) {
        Device = device;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    private static Config config;


    public static Config getConfig(){
        if(config == null)
            config = new Config();

        return config;
    }
    private  Config(){
        {
            try {
                configNode = objectMapper
                        .readTree(new File(getClass().getClassLoader().getResource("uat.json").getPath()));
                if(configNode.has("username"))
                    setUserName(configNode.get("username").asText());
                if(configNode.has("password"))
                    setPassword(configNode.get("password").asText());

                // from System.Environment
                Browser = System.getenv("browser")!= null ? System.getenv("browser") : "chrome";
                Device = System.getenv("device")!= null ? System.getenv("device") : "desktop";



            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
